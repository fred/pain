ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

1.5.0 (2024-05-13)
------------------

* Drop support for Django 3.2 (#4)

1.4.0 (2024-03-18)
------------------

* Update project settings (Python 3.8 to 3.11, Django 3.2, 4.2) (#1, #2)
* Adapt for use with ``django-pain`` 3.0 (#3)
