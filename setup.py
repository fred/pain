#!/usr/bin/python3
#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Setup script for fred_pain."""

from distutils.command.build import build

from setuptools import setup
from setuptools.command.sdist import sdist


class custom_build(build):
    """Custom build."""

    sub_commands = [("compile_catalog", lambda x: True)] + build.sub_commands


class custom_sdist(sdist):
    """Custom sdist."""

    def run(self):
        """Implementation of run."""
        self.run_command("compile_catalog")
        # sdist is an old style class so super cannot be used
        sdist.run(self)

    def has_i18n_files(self):
        """Check for internationalization files."""
        return bool(self.distribution.i18n_files)


setup(cmdclass={"build": custom_build, "sdist": custom_sdist})
